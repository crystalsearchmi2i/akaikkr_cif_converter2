# -*- coding: utf-8 -*-
import sys 
import copy

class Reader:
    """
       cif = liteCif("ciffile")

       output: 
       cif.dic[_symmetry_Int_Tables_number]
       cif.atoms[filed] = ...
    """


    # cif labels to read 
    field1 = [ "_symmetry_Int_Tables_number" ,
              "_cell_length_a",
              "_cell_length_b",
              "_cell_length_c",
              "_cell_angle_alpha",
              "_cell_angle_beta",
              "_cell_angle_gamma",]
    field2 = [ "_atom_site_label" ,"_atom_site_fract_x", 
              "_atom_site_fract_y", "_atom_site_fract_z",
             "_atom_site_Wyckoff_symbol" ]

    def __init__(self,filename):
        """ input: filename to read """
        self.lines = self.readfile(filename)
        self.parser()

    def __str__(self):
        strs = []

        str2 = copy.deepcopy(self.field1)
        str2.extend([ self.dic[y] for y in self.field1 ])
        strs.append( " ".join(str2) )

        str2 = [ y for y in self.field2 ]
        strs.append( " ".join(str2) )
        for x in self.atoms:
            str2 = [ x[y] for y in self.field2 ]  
            str2 = map(str,str2)
            strs.append( " ".join(str2) )
        return "\n".join(strs)

    @staticmethod
    def readfile(filename):
        """ input : filename 
            output: 改行を除いたstring list """
        with open(filename,"r") as f:
            lines = f.readlines()
        lines = [ line.rstrip() for line in lines ] 
        return lines


    @staticmethod
    def add_atomHelper(s,label_order):
        """ helper function,
            input: dic 
                   label_order， labelのlist
            label_orderの順にdic内のlistのdicを返す
        """
        dic = {}
        n = len(label_order)
        for i in range(n):
            x = s[i]
            dic[ label_order[i] ] = x 
        return dic 

    def parser_loop(self,lines_iter):
            """ 
              input: file の内容をstring listのiteratorで渡す
              output: self.atomsにlistを作成する。
            """
              
            label_order = []
            start_pos = False
            while True:
               line = lines_iter.next()
               s = line.split()
               if len(s)==0:
                   continue
               label = s[0]
               if not start_pos :
                 if label in self.field2:
                   label_order.append( label )
                 else: 
                   start_pos = True
                   self.atoms.append( self.add_atomHelper(s,label_order) )
               else:
                   self.atoms.append( self.add_atomHelper(s,label_order) )


    def parser(self):
        """
             main routine

             self.dic : lattice parameterなど
             self.atoms list  を返す

        """
        self.dic = {}
        self.atoms = []
        lines_iter = iter(self.lines)
        while True:
            lines = lines_iter.next()
            s = lines.split()
            if len(s) == 0:
                continue
            label = s[0]
            if label in self.field1:
                self.dic[label]=s[1] 
            elif label == "loop_":
                try:
                    self.parser_loop(lines_iter) 
                except StopIteration:
                    break
            else:
                print  "unknown label", label
                raise Exception("unknown label")

if __name__ == "__main__" :
   cif = Reader(sys.argv[1])
   print cif
